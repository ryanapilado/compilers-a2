import subprocess

expected_values_file = open("expected-values.txt", 'r')
expected_values = [value.strip() for value in expected_values_file]

for i in range(len(expected_values)):
    subprocess.run(["gcc", "test-" + str(i) + ".S"])
    proc = subprocess.run(["./a.out"], stdout=subprocess.PIPE)
    exit_code = proc.returncode
    outcome = "PASS" if str(exit_code) == expected_values[i] else "!!FAIL!!"
    print("Test " + str(i) + ": expected " + expected_values[i] + ", actual " + str(exit_code) + " ... " + outcome)